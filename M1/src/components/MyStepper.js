import React, {Component} from 'react';
import {View, Text} from 'react-native';
import MyButton from './MyButton.js';


export default class MyStepper extends Component {
	constructor(props){
		super(props)
		this.backgroundColor = this.props.backgroundColor ? this.props.backgroundColor : 'white'
		this.color = this.props.color ? this.props.color : 'blue'
		this.style = this.props.style ? this.props.style : {}
		
		this.minValue = this.props.minValue ? this.props.minValue : 0
		this.maxValue = this.props.maxValue ? this.props.maxValue : 100
		this.text = this.props.text ? this.props.text : "Stepper"

		const currentValue = this.props.currentValue ? this.props.currentValue : 50
		this.state = {currentValue: currentValue}
	}
	buttonPressed(sender){
		let val = this.state.currentValue
		if (sender.text == "-") { val--; }
		else { val++; }

		if (val < this.minValue) { val = this.minValue; }
		if (val > this.maxValue) { val = this.maxValue; }

		this.setState({currentValue: val})
		if (this.props.onValueChange) { this.props.onValueChange(this, val); }
	}
	
	render() {
		return(
			<View style={{flex:1, ...this.style, backgroundColor: this.backgroundColor}}>
				<Text style={{flex:1, textAlign: 'center', color: this.color}}>{this.text}</Text>
				<View style={{flex:1, flexDirection: 'row', alignItems: 'stretch', backgroundColor: this.backgroundColor, borderColor: this.color, borderWidth: 1, borderRadius: 5}}>
					<MyButton onPress={this.buttonPressed.bind(this)} style={{flex:1, backgroundColor: this.color, borderColor: this.color, margin: 0}} textStyle={{color: this.backgroundColor, fontSize: 15}}>-</MyButton>
					<Text style={{flex:1, textAlign: 'center', textAlignVertical: 'center'}}>{this.state.currentValue}</Text>
					<MyButton onPress={this.buttonPressed.bind(this)} style={{flex:1, backgroundColor: this.color, borderColor: this.color, margin: 0}} textStyle={{color: this.backgroundColor, fontSize: 15}}>+</MyButton>
				</View>
			</View>
		);
	}
}
