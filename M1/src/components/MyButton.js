import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';


// http://bit.ly/argitlab
export default class MyButton extends Component {
	constructor(props){
		super(props)

		this.text = this.props.children
		this.buttonStyle = {
            borderColor: 'black',
            borderWidth: 1,
            margin: 5,
            backgroundColor: 'lightgray',
            justifyContent: 'center',
            alignItems: 'center',
        }
        this.textStyle = {
            textAlign: 'center',
            fontSize: 20,
        }

		if (this.props.style) { this.buttonStyle = {...this.buttonStyle, ...this.props.style}; }
		if (this.props.textStyle) { this.textStyle = {...this.textStyle, ...this.props.textStyle}; }
	}
	buttonPressed(){
		if (this.props.onPress) { this.props.onPress(this); }
	}
	
	render() {
		return(
			<TouchableOpacity onPress={this.buttonPressed.bind(this)} style={this.buttonStyle}>
				<Text style={this.textStyle}>{this.text}</Text>
			</TouchableOpacity> 
		);
	}
}
