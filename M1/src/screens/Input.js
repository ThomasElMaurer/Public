import React, {Component} from 'react';
import {StyleSheet, View, Text, TextInput, TouchableOpacity} from 'react-native';

class MySegmentControl extends Component {
    constructor(props) {
        super(props)
        this.style = this.props.style ? this.props.style : {}
        this.values = this.props.values ? this.props.values : []
        this.borderRadius = this.style.borderRadius ? this.style.borderRadius : 5
        this.activeColor = this.props.activeColor ? this.props.activeColor : 'blue'
        this.inactiveColor = this.props.inactiveColor ? this.props.inactiveColor : 'white'
        let selected = this.props.selectedIndex ? this.props.selectedIndex : 0


        this.state = {selectedIndex: selected}

        this.selectionChange = this.selectionChange.bind(this)          
    }
    selectionChange(index){
        if (this.state.selectedIndex != index) {
            this.setState({selectedIndex: index})
            if (this.props.onSelectionChange) { this.props.onSelectionChange(index); }
    }   }
    
    render() {
        const buttons = []
        for(let i = 0; i < this.values.length; i++)  {
            let activeColor = i == this.state.selectedIndex ? this.inactiveColor : this.activeColor
            let inactiveColor = i == this.state.selectedIndex ? this.activeColor : this.inactiveColor
            
            let style = {flex: 1, justifyContent: 'center', alignItems: 'center', borderColor: this.activeColor, borderWidth: 1, backgroundColor: inactiveColor}
            if (i == 0) {
                style = {...style, borderBottomLeftRadius: this.borderRadius, borderTopLeftRadius: this.borderRadius}
            }
            if (i == this.values.length -1) {
                style = {...style, borderBottomRightRadius: this.borderRadius, borderTopRightRadius: this.borderRadius}
            }
            buttons.push(<TouchableOpacity style={style} key={i} onPress={() => this.selectionChange(i)}><Text style={{color: activeColor}}>{this.values[i]}</Text></TouchableOpacity>)
        }
        return(
            <View style={{flexDirection: 'row', backgroundColor: this.inactiveColor, borderRadius: this.borderRadius, ...this.style}}>
                {buttons}
            </View>
        );
    }
}

export default class Input extends Component {
	constructor(props) {
		super(props)
		this.state = {text: ""}
    }
	render() {
        
		return (
			<View style={styles.container}>
				<View style={styles.top}>
                    <Text style={styles.title}>List</Text>
				</View>
                <View style={styles.mid}>
                    <View style={styles.bar}>
                        <MySegmentControl style={{flex: 1, margin: 5}} values={["Toto", "Titi", "Tutu"]}/>
                    </View>
				</View>
			</View>
			);
		}
	}
	
	const styles = StyleSheet.create({
		container: {
			flex: 1,
			flexDirection: 'column',
			backgroundColor: '#F5FCFF',
		},
		top: {
			flex: 1,
            justifyContent: 'flex-end',
        },
		mid: {
			flex: 6,
            flexDirection: 'column',
        },
        bar: {
            height: 50,
            backgroundColor: 'lightgray',
        },
		title: {
			fontSize: 25,
			fontWeight: 'bold',
			textAlign: 'center',
			marginBottom: 10,
		},
		
	});
	