import React, {Component} from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native';
import MyButton from '../components/MyButton.js';
import MyStepper from '../components/MyStepper';


export default class Youpi extends Component {
	constructor(props) {
		super(props)
		this.state = { colors: this.generateColor(25, 25), lines: 25, strips: 25 }
		this.loop = false
		this.speed = 50
	}

	buttonPressed(){
		this.loop = !this.loop
		if (this.loop) {
			this.interval = setInterval(this.changeColor.bind(this),this.speed)
		}
		else {
			clearInterval(this.interval)
		}
	}
	
	generateColor(strips, lines) {
		const c = []
		for (let i = 0; i < strips; i++) {
			const l = []
			for (let j = 0; j < lines; j++) {
				const r = Math.floor(Math.random() * 255)
				const g = Math.floor(Math.random() * 255)
				const b = Math.floor(Math.random() * 255)
				const rgb = ' rgb('+r+','+g+','+b+')'  
				l.push(rgb)
			}
			c.push(l)
		}
		return c
	}

	changeColor() {
		this.setState({...this.state, colors: this.generateColor(this.state.strips, this.state.lines)})
	}

	stepperValueChange(sender, val) {
		if (sender.text == "Col") {
			this.setState({...this.state, strips: val, colors: this.generateColor(val, this.state.lines)})
		}
		else if (sender.text == "Row") {
			this.setState({...this.state, lines: val, colors: this.generateColor(this.state.strips, val)})
		}
		else if (sender.text == "Speed") {
			this.speed = val
			if (this.loop) {
				clearInterval(this.interval)
				this.interval = setInterval(this.changeColor.bind(this),this.speed)
			}
		}
	}
	render() {
		const array = []
		for (let i = 0; i < this.state.strips; i++) {
			const blocks = []
			for (let j = 0; j < this.state.lines; j++) {
				blocks.push((<View style={{...styles.block, backgroundColor: this.state.colors[i][j]}} key={"" + i + j}/>))
			}
			array.push((<View style={styles.line} key={"" + i}>{blocks}</View>))
		}

		return (
			<View style={styles.container}>
				<View style={styles.top}>
					<Text style={styles.title}>Youpi-léptique!</Text>
				</View>
				<View style={styles.mid}>
					{array}
				</View>
				<View style={styles.bottom}>
					<View style={{flexDirection: 'row', flex: 7}}>
						<MyStepper onValueChange={this.stepperValueChange.bind(this)} style={{margin: 3}} text="Col" currentValue={25} minValue={1} maxValue={150}/>
						<MyStepper onValueChange={this.stepperValueChange.bind(this)} style={{margin: 3}} text="Speed" currentValue={50} minValue={10} maxValue={1000}/>
						<MyStepper onValueChange={this.stepperValueChange.bind(this)} style={{margin: 3}} text="Row" currentValue={25} minValue={1} maxValue={150}/>
					</View>
					<MyButton style={styles.button}	textStyle={styles.buttonText} onPress={this.buttonPressed.bind(this)}>Launch / Stop</MyButton>
				</View>
			</View>
			);
		}
	}
	
	const styles = StyleSheet.create({
		container: {
			flex: 1,
			flexDirection: 'column',
			backgroundColor: '#F5FCFF',
		},
		top: {
			flex: 1,
			justifyContent: 'flex-end',
		},
		mid: {
			flex: 6,
			flexDirection: 'row',
		},
		bottom: {
			flex: 1,
		},
		title: {
			fontSize: 25,
			fontWeight: 'bold',
			textAlign: 'center',
			marginBottom: 10,
		},
		line: {
			flex: 1,
		},
		block: {
			flex: 1,
		},
		button: {
			flex: 5,
			justifyContent: 'center',
			alignItems: 'center',
			backgroundColor: 'blue',
			borderRadius: 15,
			marginBottom: 10,
			marginHorizontal: 10,
		},
		buttonText: {
			textAlign: 'center',
			fontSize: 20,
			fontWeight: 'bold',
			color: 'white',
		}
	});
	