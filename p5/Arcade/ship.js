class Ship {

    constructor() {
        this.pos = createVector(width/2, height / 2);
        this.radius = 20;
        this.velocity = createVector(0,0);
        this.angle = 0;
        this.isDead = false;
        this.points = 0;
        this.bullets = [];
    }

    render() {
        push();
        translate(this.pos.x, this.pos.y);
        rotate(this.angle + TWO_PI / 4);
        triangle(0, -this.radius, -this.radius/2, this.radius, this.radius/2, this.radius);
        pop();

        for(let i = 0; i < this.bullets.length; i++) {
            this.bullets[i].render();
        }
    }

    update() {
        this.pos.add(this.velocity);
        this.velocity.mult(0.99);
        this.edges();

        for(let i = this.bullets.length -1; i >= 0 ; i--) {
            this.bullets[i].update();
            if (this.bullets[i].offscreen()) {
                this.bullets.splice(i, 1);
            }
        }
    }

    edges() {
        if (this.pos.x < 0) { this.pos.x = width ;}
        if (this.pos.x > width) { this.pos.x = 0 ;}
        if (this.pos.y < 0) { this.pos.y = height ;}
        if (this.pos.y > height ) { this.pos.y = 0 ;}
    }

    move(key) {
        if (key =="w") {
            let v = createVector(1, 0);
            v.rotate(this.angle);
            this.velocity.add(v);
        }
        if (key =="d") {
            this.angle += 0.05;
        }
        if (key =="a") {
            this.angle -= 0.05;
        }
        this.velocity.limit(5);
    }

    shoot() {
        let b = new Bullet(this.pos, this.angle);
        this.bullets.push(b);
    }

    megaBlast() {
        let c = floor(random(1, 360));
        for (let a = 0; a < TWO_PI;  a += TWO_PI/ c) {
            let b = new Bullet(this.pos, a);
            this.bullets.push(b);
        }
    }

    collide(asteroids) {
        let newAsteroids = []
        for(let i = asteroids.length -1; i >= 0; i--) {
            let destroyed = false;
            for(let j = this.bullets.length -1; j >= 0; j--) {
                if (this.bullets[j].collide(asteroids[i])) {
                    this.bullets.splice(j, 1);
                    let pieces = asteroids[i].divide();
                    asteroids.splice(i, 1);

                    newAsteroids = newAsteroids.concat(pieces);
                    destroyed = true;
                    this.points += 1;
                    break;
                }
            }
            if (!destroyed) {
                let d = dist(this.pos.x, this.pos.y, asteroids[i].pos.x, asteroids[i].pos.y);
                if (d < this.radius + asteroids[i].radius) {
                    this.isDead = true;
                }
            }
        }
        newAsteroids = newAsteroids.concat(asteroids);
        return newAsteroids;
    }

}