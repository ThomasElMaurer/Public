let canvas;
let center;
let ship;
let asteroids = [];
let asteroCount = 5;
let keys = [];

function setup() {
	center = select('#b5');
	let size = center.size();
	canvas = createCanvas(800, 600);
	canvas.parent(center);
	ship = new Ship();
	setupAsteroids();
}


function setupAsteroids() {
	// ship = new Ship();

	for (let i = 0; i < asteroCount; i++) {
		let a = new Asteroid();
		asteroids.push(a);
	}
}

function draw() {
	background(0);
	let scoreH1 = select("#score");
	console.log(scoreH1);
	scoreH1.html("Score: " + ship.points);
	ship.render();
	for (let i = 0; i < asteroids.length; i++) {
		asteroids[i].render();
		asteroids[i].update();
	}


	for (let i = 0; i < keys.length; i++) {
		ship.move(keys[i]);
	}
	ship.update();
	asteroids = ship.collide(asteroids);

	// if (ship.isDead) {
	// 	ship = undefined;
	// 	asteroids = [];
	// 	asteroCount = 5;
	// 	setupAsteroids();
	// }
	// else 
	if (asteroids.length == 0) {
		asteroCount += 2;
		setupAsteroids();
	}
}

function keyPressed() {
	if (key == " ") {
		ship.shoot();
	}
	else if (key == "p") {
		ship.megaBlast();
	}
	else {
		keys.push(key);
	}
}

function keyReleased() {
	let i = keys.indexOf(key);
	if(i > -1) {
		keys.splice(i, 1);
	}
}

function windowResized() {
	let size = select("#b1").size();
	// resizeCanvas(size.width, size.height);
}
