class Bolt {
    constructor(parent) {
        this.points = [];
        this.childs = [];
        this.splitProba = 1;
        this.reached = false;
        if (parent) {
            this.parent = parent;
            this.parent.addChild(this);
            this.points.push(this.parent.end().copy());
            this.dir = this.parent.dir.copy();
            this.splitFactor = this.parent.splitFactor * 1.5;
            this.growthFactor = this.parent.growthFactor * 0.9;
        }
        else { 
            this.parent = undefined; 
            let w = width / 4;
            this.points.push(createVector(random(w, w*3) / 2, 0));
            this.dir = createVector(0, random(1, 1));
            this.splitFactor = splitFactor;
            this.growthFactor = growthFactor;
        }

    }

    end() { return this.points[this.points.length -1]; }
    start() { return this.points[0]; }

    addChild(child) { this.childs.push(child); }
    removeChild(child) { this.childs.splice(this.childs.indexOf(child), 1); }

    isDone() {
        if (this.parent) { return this.parent.isDone(); }
        return this.reached;
    }
    setIsDone() {
        if (this.parent) { this.parent.setIsDone(); }
        this.reached = true;
    }

    render() {
        for (let i = 0; i < this.points.length; i++) {
            if (i > 0) {
                let p1 = this.points[i];
                let p2 = this.points[i -1];
                let w = this.reached ? 8 : 4;
                stroke(255, 75);
                strokeWeight(w * 4);
                line(p1.x, p1.y, p2.x, p2.y);

                stroke(255, 125);
                strokeWeight(w * 3);
                line(p1.x, p1.y, p2.x, p2.y);

                stroke(255, 175);
                strokeWeight(w * 2);
                line(p1.x, p1.y, p2.x, p2.y);

                stroke(255, 255);
                strokeWeight(w);
                line(p1.x, p1.y, p2.x, p2.y);

                
                if (p1.y > height) { this.setIsDone(); }
                else if (p1.y > height*0.80) { this.points.push(createVector(p1.x, height+10)); }
            }
        }
        for (let i = 0; i < this.childs.length; i++) { this.childs[i].render(); }

        if (!this.parent) { this.renderCenter(); }
    }

    renderCenter() {
        if (this.parent && this.parent.reached && !this.reached) { return; }
        for (let i = 0; i < this.points.length; i++) {
            if (i > 0) {
                let p1 = this.points[i];
                let p2 = this.points[i -1];

                let w = this.reached ? 8 : 4;
                
                stroke(0, 0, 200, 20);
                strokeWeight(w * 3);
                line(p1.x, p1.y, p2.x, p2.y);

                stroke(0, 0, 200, 40);
                strokeWeight(w * 2);
                line(p1.x, p1.y, p2.x, p2.y);

                stroke(0, 0, 200, 60);
                strokeWeight(w);
                line(p1.x, p1.y, p2.x, p2.y);

                
                if (p1.y > height) { this.setIsDone(); }
            }
        }
        for (let i = 0; i < this.childs.length; i++) { this.childs[i].renderCenter(); }
    }

    grow() {
        if (this.childs.length == 0) {
            let angle = splitAngle;
            if (this.points.length == 1) { angle = splitAngle * 3; }
            let last = this.end().copy();
            this.dir.rotate(random(-angle, angle));
            if (this.dir.y < 0.25) { this.dir.y = 0.25; }
            let gf = random(this.growthFactor/2, this.growthFactor);
            let dir = this.dir.copy().mult(gf);
            last.add(dir);
            this.points.push(last);
            this.split();
        }
        else { for (let i = 0; i < this.childs.length; i++) { this.childs[i].grow(); } }
    }

    split() {
        if (this.childs.length == 0) {
            let split = floor(random(1, this.splitProba));
            if (split % this.splitFactor  == 0) {
                let childsCount = random(1, 4);
                for (let i = 0; i < childsCount; i++) {
                    let child = new Bolt(this);
                }
            }
            else { this.splitProba += 1; }
        }
    }

    removeDeadLeafs() {
        let flag = false;
        if (this.childs.length > 0) {
            for (let i = 0; i < this.childs.length; i++) {
                if (this.childs[i].removeDeadLeafs()) { flag = true; }
            }
        }
        else {
            if (!this.reached) {
                flag = true;
                this.parent.removeChild(this);
                this.parent = undefined;
            }
        }
        return flag;
    }
}