const growthFactor = 100;
const splitFactor = 2;
const splitAngle = 0.5;
const dropSpeed = 100;
const dropSize = 10;
const fadeInMaxDuration = 5;
const fadeOutMaxDuration = 60;
const idleMaxDurtion = 360;


let bolts;
let fadeInCount = 0;
let fadeInCountMax = 0;
let fadeOutCount = 0;
let fadeOutCountMax = 0;
let idleCount = 0;

let sounds = [];
let rainSounds = [];
let started = false;

let drops = [];
let div;

function preload() {
	soundFormats('mp3', 'ogg');
	for (let i = 1; i <= 4; i++) {
		let sound = loadSound('thunder/' + i +  '.mp3');
		sounds.push(sound);
	}
	for (let i = 1; i <= 3; i++) {
		let sound = loadSound('rain/' + i +  '.mp3');
		rainSounds.push(sound);
	}
}

function setup() {
	createCanvas(windowWidth, windowHeight);
	makeBolts();
	for (let i = 0; i < 1000; i++) {
		drops.push(new Drop());
	}
	div = createDiv();
	div.id("content");

	let button = createButton("Or click here");
	button.parent(div);
	let x = (width/25) * 2;
	button.style('margin-top', x + 'px');
	button.mousePressed(start);
}

function draw() {
	if (!started) {
		background(0);
		textSize(width/25);
		textAlign(CENTER,CENTER);
		noStroke();
		fill(255);
		text("Tap spacebar to start the storm", width/2, height/2);
		noLoop();
	}
	else if (idleCount > 0) {
		background(0);
		rain()
		idleCount--;
		if (idleCount == 0) { makeBolts(); }
	}
	else if (fadeInCount > 0) {
		let c = map(fadeInCount, 0, fadeInCountMax, 255, 0);
		background(c);
		rain()
		for (let i = 0; i < bolts.length; i++) { bolts[i].render() ;}
		fadeInCount--;
		if (fadeInCount == 0) {
			fadeOutCount = floor(random(20, fadeOutMaxDuration));
			fadeOutCountMax = fadeOutCount;
			bolts = [];
		}
	}
	else if (fadeOutCount > 0) {
		let c = map(fadeOutCount, 0, fadeOutCountMax, 0, 255);
		background(c, c);
		rain()
		fadeOutCount--;
		if (fadeOutCount == 0) { idleCount = floor(random(0, idleMaxDurtion));  }
	}
	else {
		background(0);
		rain()
		for (let i = 0; i < bolts.length; i++) {
			let bolt = bolts[i];
			noFill();
			stroke(255);
			
			bolt.render();
			if (!bolt.isDone()) {
				bolt.grow();
			}
			else {
				if(!bolt.removeDeadLeafs()) {
					thunder();
					fadeInCount = floor(random(2, fadeInMaxDuration));
					fadeInCountMax = fadeInCount;
				}
			}
		}
	}
	
}

function rain() {
	for (let i = 0; i < drops.length; i++) {
		drops[i].render();
		drops[i].update();
	}
}

function makeBolts() {
	let r = random(1, 1000);
	let c = 1;
	if (r > 970) {
		c = 3;
	}
	else if (r > 900) {
		c = 2;
	}

	bolts = [];
	for (let i = 0; i < c; i++) {
		bolts.push(new Bolt());
	}
}

function start() {
	for (let i = 0; i < rainSounds.length; i++) {
		let sound = rainSounds[i];
		sound.setVolume(0.1);
		sound.loop();
	}
	started = true;
	loop();
	div.remove();
	div = undefined;
}

function stop() {
	for (let i = 0; i < rainSounds.length; i++) {
		let sound = rainSounds[i];
		sound.setVolume(0.1);
		sound.stop();
	}
	started = false;
	div = createDiv();
	div.id("content");

	let button = createButton("Or click here");
	button.parent(div);
	let x = (width/25) * 2;
	button.style('margin-top', x + 'px');
	button.mousePressed(start);
}

function thunder() {
	let r = floor(random(sounds.length -1));
	let sound = sounds[r];
	sound.setVolume(1);
	sound.play();
}

function keyPressed() {
	if (key == " ") {
		if (started) { stop(); }
		else { start(); }
	}
}

function windowResized() {
	resizeCanvas(windowWidth, windowHeight);
}
