class Star {
    constructor() {
        this.pos = createVector(0, 0);
        
        let x = random(-maxSpeed, maxSpeed);
        let y = random(-maxSpeed, maxSpeed);
        this.velocity = createVector(x, y);
        this.update();
    }

    render() {
        if (this.size > 0) {
            push();
            noStroke();
            fill(255);
            ellipse(this.pos.x, this.pos.y, this.size, this.size);
            pop();
        }
    }

    update() {
        let d = dist(0, 0, this.pos.x, this.pos.y);
        this.size =  map(d, 0, maxD, -1, 20);
        let speed = map(d, 0, maxD, 0.1, 1);

        let v = this.velocity.copy();
        v.mult(speed);
        this.pos.add(v);
        this.isOff();
    }

    isOff() {
        if (this.pos.x > width ||  this.pos.x < -width || this.pos.y > height ||  this.pos.y < -height) {
            this.pos = createVector(0, 0);
        } 
    }
}