const totalStars = 10;
const maxSpeed = 25;
const screenSize = 800;

let maxD;
let stars = [];

function setup() {
	createCanvas(screenSize, screenSize);
	maxD = dist(0, 0, width, height);
	for (let i = 0; i < totalStars; i++) {
		let star = new Star();
		stars.push(star);
	}

}

function draw() {
	background(0);
	translate(width/2, height/2);
	for (let i = 0; i < 10; i++) {
		stars.push(new Star());
	}
	for (let i = 0; i < stars.length; i++) {
		let star = stars[i];
		star.render();
		star.update();
	}


	fill(0);
	noStroke();
	// ellipse(0, 0, 25, 25);
	
}

