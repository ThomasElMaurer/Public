let canvasMouseIsPressed = false; 
let shapes = [];
let history = [];
let currentShape;
let author = "JeanBagarre";
let drawings = [];
let drawingsList;


let size, strokeColor, strokeState, fillColor, fillState, selectedTool;

function setup() {
	let canvas = createCanvas(600, 600);
	loadAll();
	canvas.parent('#canvas');
	canvas.mousePressed(canvasMousePressed);
	canvas.mouseReleased(canvasMouseReleased);
	select("#send").mousePressed(sendData);
	select('#load').mousePressed(loadData);
	select('#clear').mousePressed(clearData);

	select("#undo").mousePressed(undo);
	select("#redo").mousePressed(redo);

	select("#toolFree").mousePressed(() => { selectTool(0); });
	select("#toolRect").mousePressed(() => { selectTool(1); });
	select("#toolCircle").mousePressed(() => { selectTool(2); });
	
	size = 4;
	strokeColor = color(255, 255, 255);
	selectedTool = 0;
	strokeState = true;
	fillState = true;
}

function draw() {
	updateDisplay();
	background(0);
	if (canvasMouseIsPressed) { addPoint(mouseX, mouseY); }
	
	for(let j = 0; j< shapes.length; j++) {
		let shape = shapes[j];
		if (shape.strokeColor) { stroke(shape.strokeColor.r, shape.strokeColor.g, shape.strokeColor.b); }
		else { noStroke(); }
		strokeWeight(shape.size);

		if (shape.fillColor) { fill(shape.fillColor.r, shape.fillColor.g, shape.fillColor.b); }
		else { noFill(); }
		
		if (shape.points) {
			noFill();
			let points = shape.points;
			beginShape();
			for (let i = 0; i < points.length; i++) {
				let p = points[i];
				vertex(p.x, p.y);
			}
			endShape();
		}
		else if (shape.rect) {
			let rectangle = shape.rect;
			rect(rectangle.origin.x, rectangle.origin.y, rectangle.size.w, rectangle.size.h);
		}
		else if (shape.circle) {
			let circle = shape.circle;
			let w = circle.size.w;
			let h = circle.size.h;

			let x = circle.origin.x + w/2;
			let y = circle.origin.y + h/2;

			ellipse(x,y,w,h);
		}
	}
	
}

function addPoint(x, y) {
	if (selectedTool == 0) {
		currentShape.points.push({x: x, y: y});
	}
	else if (selectedTool == 1) {
		let o = currentShape.rect.origin;
		currentShape.rect.size.w = mouseX - o.x;
		currentShape.rect.size.h = mouseY - o.y;
	}
	else if (selectedTool == 2) {
		let o = currentShape.circle.origin;
		currentShape.circle.size.w = mouseX - o.x;
		currentShape.circle.size.h = mouseY - o.y;
	}
}

// Display
function updateDisplay() {
	// Size
	size = select("#sizeSlider").value();
	select("#sizeDisplay").value(size)

	// Stroke
	let sR = select("#strokeR").value();
	let sG = select("#strokeG").value();
	let sB = select("#strokeB").value();
	strokeColor = {r: sR, g: sG, b: sB};
	select("#strokeDisplay").style("background-color", color(sR, sG, sB));

	select("#strokeRDisplay").value(sR);
	select("#strokeGDisplay").value(sG);
	select("#strokeBDisplay").value(sB);

	strokeState = select("#strokeState").checked();

	// fill
	let fR = select("#fillR").value();
	let fG = select("#fillG").value();
	let fB = select("#fillB").value();
	fillColor = {r: fR, g: fG, b: fB};
	select("#fillDisplay").style("background-color", color(fR, fG, fB));

	select("#fillRDisplay").value(fR);
	select("#fillGDisplay").value(fG);
	select("#fillBDisplay").value(fB);

	let tool = "Selected tool: "
	if (selectedTool == 0) { tool += "Freeform"; }
	if (selectedTool == 1) { tool += "Rect"; }
	if (selectedTool == 2) { tool += "Circle"; }
	select("#selectedTool").html(tool);

	fillState = select("#fillState").checked();
}

function showList() {
	if (drawingsList) {
		drawingsList.remove();
	}
	drawingsList = createElement("ul");
	drawingsList.parent("#list");

	for (let i = 0; i < drawings.length; i++) {
		let d = drawings[i];
		let li = createElement("li");
		li.parent(drawingsList);
		li.html(d.id + " by "+ d.author);
		li.mousePressed(() => { selectLi(d.id); });
	}
}

function selectLi(id) {
	loadDataWithId(id);
}

function clearData() {
	shapes = [];
	loadAll();
}

// Action Handles
function canvasMousePressed() {
	canvasMouseIsPressed = true;
	let points = selectedTool == 0 ? [] : undefined;
	let rect = undefined;
	let circle = undefined;
	if (selectedTool == 1) {
		rect = {
			origin: {
				x: mouseX,
				y: mouseY
			},
			size : {
				w: 0, 
				h: 0
			}
		};
	}
	else if (selectedTool == 2) {
		circle = {
			origin: {
				x: mouseX,
				y: mouseY
			},
			size : {
				w: 0, 
				h: 0
			}
		};
	}

	currentShape = { 
		points: points,
		rect: rect,
		circle: circle,
		size: size,
		strokeColor: strokeState || selectedTool == 0 ? strokeColor : undefined,
		fillColor: fillState ? fillColor : undefined
	};
	shapes.push(currentShape);
}

function canvasMouseReleased() {
	canvasMouseIsPressed = false;
}

function selectTool(toolId) {
	selectedTool = toolId
}

function undo() {
	if (shapes.length > 0) {
		let shape = shapes[shapes.length - 1];
		shapes.splice(shapes.length -1, 1);
		history.push(shape);
	}
}

function redo() {
	if (history.length > 0){
		let shape = history[history.length - 1];
		history.splice(history.length -1, 1);
		shapes.push(shape);
	}
}
// Network
function sendData() {
	fetch('http://cours.ardev.info/paint/index.php?action=send&author=' + author, {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(shapes)
	})
	.then( response => {
		return response.json();
	})
	.then(responseJson => {
		select("#savingState").html("Saved with id: " + responseJson.id);
		loadAll();
	})
}

function loadData() {
	let id = select("#id").value();
	loadDataWithId(id);
}

function loadDataWithId(id) {
	loadAll();
	fetch('http://cours.ardev.info/paint/index.php?action=getById&id=' + id)
	.then( response => {
		return response.json();
	})
	.then(responseJson => {
		let d = responseJson.data;
		return JSON.parse(d);
	})
	.then(r => {
		shapes = r;
		console.log(r);
	})
}

function loadAll() {
	fetch('http://cours.ardev.info/paint/index.php?action=getAll')
	.then( response => {
		return response.json();
	})
	.then(responseJson => {
		drawings = responseJson;
		showList();
	})
}
