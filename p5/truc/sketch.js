const totalPoints = 10000;
const neighbourCount = 50;

let points = [];
let paths = [];

let p;

function setup() {
	createCanvas(800, 800);

	for (let i = 0; i < totalPoints; i++) {
		let x = random(width);
		let y = random(height);
		let v = createVector(x, y);
		points.push(v);
	}
	p = createP("");
}

function draw() {
	background(0);
	findNextPath();
	noFill();
	stroke(255);
	strokeWeight(2);
	for (let i = 0; i < points.length; i++) {
		let v = points[i];
		point(v.x, v.y);
	}

	for (let i = 0; i < paths.length; i++) {
		let o = paths[i];

		strokeWeight(4);
		point(o.center.x, o.center.y);
		beginShape();
		strokeWeight(1);
		for (let j = 0; j < o.points.length; j++) {
			let p = o.points[j];
			line(o.center.x, o.center.y, p.x, p.y);
			// vertex(p.x, p.y);
		}
		endShape(CLOSE);
	}

	

	p.html(frameRate() + "<br\>" + millis());
	if (points.length <= 0) {
		noLoop();
	}
}


// {
// 	center: "Vector",
// 	points: "[Vector]"
// }

function findNextPath() {
	if (points.length <= 0) { return; }
	let centerIndex = floor(random(points.length - 1));
	let center = points[centerIndex];
	let otherPoints = [];
	points.splice(centerIndex, 1);


	while (otherPoints.length < neighbourCount && points.length > 0) {
		let smallest = pow(width * height, 2);
		let biggest = 0;
		let otherIndex = 0;
		for (let i = 0; i < points.length; i++) {
			let v = points[i];
			let d = squaredDist(center.x, center.y, v.x, v.y);
			// if (d < smallest) {
			// 	smallest = d;
			// 	otherIndex = i;
			// }
			if (d > biggest) {
				biggest = d;
				otherIndex = i;
			}
		}

		let other = points[otherIndex];
		points.splice(otherIndex, 1);
		otherPoints.push(other);
	}

	let o = {
			center: center,
			points: otherPoints
		};
		paths.push(o);
}


function squaredDist(x1, y1, x2, y2) {
	let a = abs(x1 - x2);
	let b = abs(y1 - y2);
	let c = pow(a, 2) + pow(b, 2);
	return c;
}