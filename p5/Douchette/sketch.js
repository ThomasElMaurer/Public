let csvData;
let inputs;
let idInput;

function preload() {
	csvData = loadTable('data.csv', 'csv', 'header');
}

function setup() {
	noCanvas();
	select("#save").mousePressed(saveData);
	
	createAddPanel();
	displayTable();
	idInput.elt.focus();
}

function createAddPanel() {
	let panel = select("#addPanel");
	inputs = [];
	for (let i = 0; i < csvData.columns.length -1; i++) {
		let name = csvData.columns[i];
		let input = createInput("", "text");
		input.attribute("placeholder", name);
		input.parent(panel);
		inputs.push(input);
	}
	let button = createButton(" + ");
	button.parent(panel);
	button.mousePressed(addToCsv);

	
	let p = createP("OR");
	p.parent(panel);

	idInput = createInput("", "text");
	idInput.attribute("placeholder", "SCAN");
	idInput.parent(panel);
}

function displayTable() {
	let table = select("#csvTable");
	table.html("");

	let header = createElement("tr"); 
	header.parent(table);
	// csvData.columns
	for (let i = 0; i < csvData.columns.length; i++) {
		let th = createElement("th"); 
		th.parent(header);
		th.html(csvData.columns[i]);
	}
	let th = createElement("th"); 
	th.parent(header);
	th.html("Total");

	
	for (let i = 0; i < csvData.rows.length; i++) {
		let row = csvData.rows[i];
		let tr =  createElement("tr"); 
		tr.parent(table);
		for (let j = 0; j < row.arr.length; j++) {
			let td = createElement("td"); 
			td.parent(tr);
			td.html(row.arr[j]);
		}
		let t = int(row.arr[1]) * int(row.arr[2]);
		let td = createElement("td"); 
		td.parent(tr);
		td.html(t);
	}
}

function addToCsv() {
	let newRow = csvData.addRow();
	for (let i = 0; i < inputs.length; i++) {
		let input = inputs[i];
		let name = csvData.columns[i];
		newRow.setString(name, input.value());
		input.value("");
	}	
	newRow.setString("count", "0");
	displayTable();
}

function keyPressed() {
	if (keyCode == RETURN) {
		let id = idInput.value();
		if (id && id != "") {
			for (let i = 0; i < csvData.rows.length; i++) {
				let row = csvData.rows[i];
				let rowId = row.arr[0];
				if (id == rowId) {
					let c = int(row.arr[2]) + 1;
					row.arr[2] = "" + c; 
					idInput.value("");
					displayTable();
					break;
				}
			}
		}
	}
}

function saveData() {
	saveTable(csvData, 'data.csv', 'csv', 'header');
}
