<?php
    class DatabaseManager {
        /* 
            Dans cette classe, vous devez implementer toutes les fonctions s'adressant directement à la base de donnée.
            Pour récuperer l'instance de PDO, utilisez simplement la methode getPdo() 
            (ex: $pdo = $this->getPdo();)
        */

        // ! Properties
        private $pdo;

        // ! Init
        public function __construct(){ }

        // ! Pdo Management
        // Ne pas modifier ces methodes
        private function getPdo() {
            if ($this->pdo == NULL) {$this->pdo = $this->connectDB();}
            return $this->pdo;
        }
        private function connectDB() {
            $dsn = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
            try {
                $db = new PDO($dsn, DB_USER, DB_PASS);
                return $db;
            } catch (PDOException $e) {
                echo "Error connecting database: " . $e->getMessage();
            }
            return false;
            
        }
        
        // ! Requests
        // Requetes nécéssaires au fonctionnement de l'application
        // A implementer
        public function getAllUsers() {
            
        }
        // A implementer
        public function getUserForId($id) {
            
        }
        // A implementer
        public function getAllMessagesFromUser($idUser) {
            
        }
        // A implementer
        public function getAllMessagesToUser($idUser) {
            
        }
        // A implementer
        public function getAllMessagesFromUserToUser($fromUserId, $toUserId) {
            
        }
        
    }
?>