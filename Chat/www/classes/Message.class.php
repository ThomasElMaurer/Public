<?php
    class Message{
        // ! Database Properties
        public $id;
        public $text;
        public $sendDate;
        public $fromUser;
        public $toUser;

        // ! Convenience Properties
        private $sender; // Devrait contenir un objet User correspondant a l'expediteur 
        private $recipient; // Devrait contenir un objet User correspondant au destinataire 
    }

?>