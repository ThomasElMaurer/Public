let list = [];
let body;

function load() {
    body = document.querySelector('body');

    fetchData({"ressource": "pokedex"}, json => {
        for (let i in json) {
            let poke = json[i];
            list.push({
                id: poke.number, 
                name: poke.name
            });
        }
        displayList();
    });
}

function fetchData(args, callback) {
    let baseURL = 'http://cours.ardev.info/Pokeflex/index.php'
    let argString = [];
    for (let k in args) {
        argString.push(k + "=" + args[k]);
    }
    if (argString.length > 0) {
        baseURL += "?"+ argString.join("&");
    }
    let req = new Request(baseURL);
    fetch(req)
    .then(response => response.json())
    .then(json => {
        callback(json);
    })
    .catch(error => {
        console.log(error);
    })
}

function cleanBody() {
    while (body.firstChild) {
        body.removeChild(body.firstChild);
    }
}

function displayList() {
    cleanBody();
    let namesList = document.createElement('ul');
    body.appendChild(namesList);

    for (let i = 0; i< list.length; i++) {
        let poke = list[i];
        let li = document.createElement('li');
        li.onclick = click;
        li.className = poke.id;
        li.innerText = capitalize(poke.name);
        namesList.appendChild(li);
    }
}

function displayPokedex(json) {
    cleanBody();
    let backButton = document.createElement('button');
    backButton.innerText = "< Back";
    body.appendChild(backButton);
    backButton.onclick = displayList;

    body.appendChild(document.createElement('br'));

    let title = document.createElement('h2');
    title.innerText = capitalize(json.name);
    body.appendChild(title);

    let img = document.createElement('img');
    img.setAttribute('src', json.image);
    img.style.cssFloat = 'left';
    body.appendChild(img);

    let p = document.createElement('p');
    body.appendChild(p);
    p.style.width = '250px'; 
    p.style.height = '100px';
    p.style.cssFloat = 'left';

    let t = [];
    for (let i = 0; i < json.types.length; i++) {
        t.push(json.types[i].name); 
    }
    p.innerText = "Types: " + t.join(", ");

    if (json.evolution) {
        p.appendChild(document.createElement('br'));
        let id = json.evolution.form.number;
        let name = json.evolution.form.name;
        let lvl = json.evolution.lvl;
        let span = document.createElement('span');
        span.className = id;
        span.onclick = click;
        span.innerText = "Will evolve to " + name + " at level " + lvl;
        p.appendChild(span);
    }


    


}


function click(e) {
    fetchData({"ressource": "pokedex", "id": e.srcElement.className}, displayPokedex);
}

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}