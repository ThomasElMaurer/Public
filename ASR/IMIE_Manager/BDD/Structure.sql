

-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  lun. 18 fév. 2019 à 16:35
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.6

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

--
-- Base de données :  `IMIE_ASR`
--
CREATE DATABASE IF NOT EXISTS `IMIE_ASR` DEFAULT CHARACTER SET utf8 ;
USE `IMIE_ASR` ;
-- --------------------------------------------------------

DROP TABLE IF EXISTS `IMIE_ASR`.`Campus` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Campus` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `zip` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Campus` (`id` ASC);

CREATE UNIQUE INDEX `name_UNIQUE` ON `IMIE_ASR`.`Campus` (`name` ASC);

CREATE UNIQUE INDEX `zip_UNIQUE` ON `IMIE_ASR`.`Campus` (`zip` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Model`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Model` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Model` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `constructor` VARCHAR(45) NOT NULL,
  `number` VARCHAR(255) NOT NULL,
  `pocesor` VARCHAR(45) NOT NULL,
  `ram` INT(11) NOT NULL,
  `hdd` INT(11) NOT NULL,
  `eth` BINARY(1) NOT NULL DEFAULT '0',
  `wifi` BINARY(1) NOT NULL DEFAULT '0',
  `year` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Model` (`id` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Computer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Computer` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Computer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idModel` INT(11) NOT NULL,
  `macEth` VARCHAR(12) NULL DEFAULT NULL,
  `macWifi` VARCHAR(12) NULL DEFAULT NULL,
  `os` VARCHAR(255) NOT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Computer_Model1`
    FOREIGN KEY (`idModel`)
    REFERENCES `IMIE_ASR`.`Model` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Computer` (`id` ASC);

CREATE UNIQUE INDEX `macEth_UNIQUE` ON `IMIE_ASR`.`Computer` (`macEth` ASC);

CREATE UNIQUE INDEX `macWifi_UNIQUE` ON `IMIE_ASR`.`Computer` (`macWifi` ASC);

CREATE INDEX `fk_Computer_Model1_idx` ON `IMIE_ASR`.`Computer` (`idModel` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Program`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Program` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Program` (
  `id` VARCHAR(3) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `maxLevel` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Program` (`id` ASC);

CREATE UNIQUE INDEX `name_UNIQUE` ON `IMIE_ASR`.`Program` (`name` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`ProgramInCampus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`ProgramInCampus` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`ProgramInCampus` (
  `idCampus` INT(11) NOT NULL,
  `idProgram` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`idCampus`, `idProgram`),
  CONSTRAINT `fk_Campus_has_Program_Campus`
    FOREIGN KEY (`idCampus`)
    REFERENCES `IMIE_ASR`.`Campus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Campus_has_Program_Program1`
    FOREIGN KEY (`idProgram`)
    REFERENCES `IMIE_ASR`.`Program` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_Campus_has_Program_Program1_idx` ON `IMIE_ASR`.`ProgramInCampus` (`idProgram` ASC);

CREATE INDEX `fk_Campus_has_Program_Campus_idx` ON `IMIE_ASR`.`ProgramInCampus` (`idCampus` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Promo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Promo` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Promo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `year` INT(11) NOT NULL,
  `idCampus` INT(11) NOT NULL,
  `idProgram` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Promo_Campus_has_Program1`
    FOREIGN KEY (`idCampus` , `idProgram`)
    REFERENCES `IMIE_ASR`.`ProgramInCampus` (`idCampus` , `idProgram`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Promo` (`id` ASC);

CREATE INDEX `fk_Promo_Campus_has_Program1_idx` ON `IMIE_ASR`.`Promo` (`idCampus` ASC);

CREATE INDEX `fk_Promo_Campus_has_Program1` ON `IMIE_ASR`.`Promo` (`idCampus` ASC, `idProgram` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Student`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Student` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Student` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(255) NOT NULL,
  `lastName` VARCHAR(255) NOT NULL,
  `idPromo` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Student_Promo1`
    FOREIGN KEY (`idPromo`)
    REFERENCES `IMIE_ASR`.`Promo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Student` (`id` ASC);

CREATE INDEX `fk_Student_Promo1_idx` ON `IMIE_ASR`.`Student` (`idPromo` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Loan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Loan` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Loan` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idComputer` INT(11) NOT NULL,
  `idStudent` INT(11) NOT NULL,
  `start` DATE NOT NULL,
  `end` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Computer_has_Student_Computer1`
    FOREIGN KEY (`idComputer`)
    REFERENCES `IMIE_ASR`.`Computer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Computer_has_Student_Student1`
    FOREIGN KEY (`idStudent`)
    REFERENCES `IMIE_ASR`.`Student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_Computer_has_Student_Student1_idx` ON `IMIE_ASR`.`Loan` (`idStudent` ASC);

CREATE INDEX `fk_Computer_has_Student_Computer1_idx` ON `IMIE_ASR`.`Loan` (`idComputer` ASC);

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Loan` (`id` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Server`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Server` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Server` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idComputer` INT(11) NOT NULL,
  `idCampus` INT(11) NOT NULL,
  `ip` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Server_Campus1`
    FOREIGN KEY (`idCampus`)
    REFERENCES `IMIE_ASR`.`Campus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Server_Computer1`
    FOREIGN KEY (`idComputer`)
    REFERENCES `IMIE_ASR`.`Computer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_Server_Computer1_idx` ON `IMIE_ASR`.`Server` (`idComputer` ASC);

CREATE INDEX `fk_Server_Campus1_idx` ON `IMIE_ASR`.`Server` (`idCampus` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Service`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Service` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Service` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE UNIQUE INDEX `id_UNIQUE` ON `IMIE_ASR`.`Service` (`id` ASC);


-- -----------------------------------------------------
-- Table `IMIE_ASR`.`Port`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `IMIE_ASR`.`Port` ;

CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`Port` (
  `idServer` INT(11) NOT NULL,
  `idService` INT(11) NOT NULL,
  `number` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`idServer`, `idService`),
  CONSTRAINT `fk_Server_has_Service_Server1`
    FOREIGN KEY (`idServer`)
    REFERENCES `IMIE_ASR`.`Server` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Server_has_Service_Service1`
    FOREIGN KEY (`idService`)
    REFERENCES `IMIE_ASR`.`Service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE INDEX `fk_Server_has_Service_Service1_idx` ON `IMIE_ASR`.`Port` (`idService` ASC);

CREATE INDEX `fk_Server_has_Service_Server1_idx` ON `IMIE_ASR`.`Port` (`idServer` ASC);

USE `IMIE_ASR` ;

-- -----------------------------------------------------
-- Placeholder table for view `IMIE_ASR`.`services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `IMIE_ASR`.`services` (`campusId` INT, `campusName` INT, `service` INT, `ip` INT, `port` INT);

-- -----------------------------------------------------
-- View `IMIE_ASR`.`services`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `IMIE_ASR`.`services` ;
DROP TABLE IF EXISTS `IMIE_ASR`.`services`;
USE `IMIE_ASR`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `imie_asr`.`services`  AS  select `c`.`id` AS `campusId`,`c`.`name` AS `campusName`,`s`.`name` AS `service`,`se`.`ip` AS `ip`,`p`.`number` AS `port` from (((`imie_asr`.`service` `s` join `imie_asr`.`port` `p` on((`s`.`id` = `p`.`idService`))) join `imie_asr`.`server` `se` on((`se`.`id` = `p`.`idServer`))) join `imie_asr`.`campus` `c` on((`c`.`id` = `se`.`idCampus`))) order by `c`.`id`,`s`.`id`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;