<?php

// http://cours.ardev.info/Pokeflex/index.php

    header("Access-Control-Allow-Origin: *");
    // ! Imports
    // We start by importing the main config script
    require_once("config/main.conf.php");

    // Then, we import the helpers.php script, that contains userfull methodes, like the one used to send DBObjects as proper JSON data
    require_once(SCRIPTS_PATH . 'helpers.php');
    
    // ! Variables creation
    // Creating DatabasManager instance
    $dm = new DatabaseManager();
    $errorMessage = NULL;


    // ! Main script
    // Checking if we recieved a ressource param (type, pokedex, pokemon, team, user or login)
    // * team is the team of pokemon associated to user
    // * login is not a real ressouce but as the login action is way different than the others, it's easier to treat it like one. For more details see Login ressource section lower in this file
    if (isset($_GET['ressource'])) {
        // formating ressource to lower to avoid typos
        $ressource = strtolower($_GET['ressource']);

        // ! Params
        //  Getting param if set
        // ? Main
        // - id: ressouce element id
        // - action: required action about the ressource (eg: create, update, delete) - get needs no action params as it's the default action

        // ? User related
        // - userId: the id of the user that own ressources
        // - pass: user's password (only useful for login ressource)
        // - token: only used for delete or update action (to ensure the user is the owner of the ressource)
        // - starter: starter pkmn number (1->bulbasaur, 2->charmander, 3->squirtle)

        // ? Update params
        // - lvl: new pokemon lvl
        // - name: new name of the pokemon
        // - order: used to manage team

        // ? Main
        $id = isset($_GET['id']) ? intval($_GET['id']) : NULL;
        $action = isset($_GET['action']) ? $_GET['action'] : "get";

        // ? User related
        $userid = isset($_GET['userId']) ? intval($_GET['userId']) : NULL;
        $pass = isset($_GET['pass']) ? $_GET['pass'] : NULL;
        $token = isset($_GET['token']) ? $_GET['token'] : NULL;
        $starter = isset($_GET['starter']) ? $_GET['starter'] : NULL;

        // ? Update params
        $xp = isset($_GET['xp']) ? intval($_GET['xp']) : NULL;
        $name = isset($_GET['name']) ? $_GET['name'] : NULL;
        $order = isset($_GET['order']) ? intval($_GET['order']) : NULL;

        // ! Filtering ressource
        // executing code that corespond to ressource
        if ($ressource == "type") {
            // ! Type
            // Here we only provide 2 actions: getting all types, or only one by it's id
            if ($id){ $element = $dm->getType($id); }
            else { $collection = $dm->getAllTypes(); }
        }
        else if ($ressource == "pokedex") {
            // ! Pokedex
            // Here we only provide 2 actions: getting all pokedex, or only one by it's id
            if ($id){ $element = $dm->getPokedex($id); }
            else { $collection = $dm->getAllPokedex(); }
        }
        else if ($ressource == "pokemon") {
            // ! Pokemon
            // Here we would not allow to get all pokemon, so if there is no parmeters, we should send an error
            // we will allow the following actions:
            // - get one pokemon, with its id
            // - get all pokemon for specific user
            // - create a pokemon for an user /----- Token required -----\
            // - update a pokemon /----- Token required -----\
            // - - update takes 3 more params :
            // - - - lvl: the new level of the pokemon
            // - - - order: new order of the pokemon
            // - - - name: new name of the pokemon

        }
        else if ($ressource == "team") {
            // ! Team
            // We only allow to get a team at a time, by it's owner id
        }
        else if ($ressource == "user") {
            // ! User
            // We allow to get all users (as a simple lists of name and id) or a detailled one by its id
            // We also allow user creation
            if ($action == "create") {
                if (!$starter) { $starter = rand(1,3);}
                if ($name && $pass) { $element = $dm->addUser($name, $pass, $starter); }
                else { $errorMessage = "Not enough arguments"; }
            }

        }
        else if ($ressource == "login") {
            // ! Login
            // This ressource will try to connect an user (using id and pass) and return a new token for it 
            if ($id && $pass) {
                $token = $dm->login($id, $pass);
                if ($token) { $element = ['token' => $token]; }
                else { $errorMessage = "Wrong credentials"; }
                
            }
            else { $errorMessage = "Not enough arguments"; }
        }
        else {
            // ! Fallback
            // If we're here, ressource type is not valid, so we set the errorMessage acrdingly
            $errorMessage = "Invalid ressource type";
        }

        // Finaly, if an element or a collection exist, we send it as a json
        if (isset($collection)) { sendObject($collection); }
        else if (isset($element)) { sendObject($element); }
    }
    else {
        // If we did not get ressource we set a value to $errorMessage
        $errorMessage = "No ressource specified";
    }



    // Checking if we have an error message to send
    if ($errorMessage) { sendObject(["Error" => $errorMessage]); }

?>