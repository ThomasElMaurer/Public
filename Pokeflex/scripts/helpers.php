<?php
    // ? This helper file will contain all usefull functions that we may need

    // ! Sending JSON
    // Main sending method
    /**
     * send an object as a json file, with the apropriate header
     * 
     * $object can be an array of DBObject or a single DBObject
     */
    function sendObject($object) {
        // Sending Header
        header('Content-Type: application/json');
        // Sending JSON
        echo json_encode(parseObject($object), JSON_PRETTY_PRINT);
    }

    // Parsing method
    /**
     * parse a DBObject or an array of them into clean JSON compatible structure
     */
    function parseObject($object) {
        if (is_a($object, 'DBObject')) { return $object->toJSON(); }
        else if (is_array($object)) {
            $array = [];
            foreach($object as $k => $o) { $array[$k] = parseObject($o); }
            return $array;
        }
        return $object;
    }
?>