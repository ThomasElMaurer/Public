<?php
    class Pokedex extends DBObject {
        // ! Class Methods
        public static function tableName() {return "Pokedex";}
        public static function idColName() {return "number";}
        
        // ! PDO Stuff 
        public $number;
        public $name;
        public $hp, $atk, $def, $speed, $spe;
        public $evolutionNumber;
        public $evolutionLvl;

        // ! Custom Stuff
        private $types;
        private $evolution;

        public function __construct() { $this->types = []; }

        public function addType($type) { $this->types[] = $type; }
        public function setEvolution($pokedex) { $this->evolution = $pokedex; }

        public function getTypes() { return $this->types; }
        public function getEvolution() { return $this->evolution; }

        

        // ! Inherited Stuff
        public function getId() { return intval($this->number); } 
        public function __toString() { return $this->getId() . ": " . $this->name . " (hp: " . $this->hp . " atk: " . $this->atk . " def: " . $this->def . " speed: " . $this->speed . " spe: " . $this->spe . ") ";}
        public function toJSON() {
            $json = [];
            $json['number'] = $this->getId();
            $json['name'] = $this->name;
            $json['stats'] = [
                'hp' => $this->hp,
                'atk' => $this->atk,
                'def' => $this->def,
                'speed' => $this->speed,
                'spe' => $this->spe
            ];

            $types = [];
            foreach($this->getTypes() as $t) {
                $types[] = $t->toJSON();
            }
            $json['types'] = $types;
            if ($this->getEvolution()) { 
                $json['evolution'] = [
                    'lvl' => $this->evolutionLvl,
                    'form' => $this->getEvolution()->toJSON()
                ]; 
            }
            $json['image'] = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' . $this->getId() . '.png';
            return $json;
        }

        //https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/
    }
?>