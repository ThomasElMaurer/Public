<?php
    class DatabaseManager {
        // ! Properties
        private $pdo;
        private $loaded;

        // ! Init
        public function __construct(){ $this->initStaticData(); }

        private function initStaticData() {
            /* 
                This method create the basic static data with all theirs relashionships loaded properly, to help further uses of the program.
                This is far from the more efficient way to do things, but in this case, as the static dataset is not very large, this strategie can be a good alternative.
                With this in place, the complexity of the data structure is way simplier.
            */

            // We begin by creating a storage for all of the static data in the manager instance
            $this->loaded = [];

            // We load types and pokedexes
            $types = $this->executeStaticSelectRequest(Type::class);
            $allPokedex = $this->executeStaticSelectRequest(Pokedex::class);
            // And we add them to the storage
            foreach($types as $t) { $this->loaded[Type::tableName()][$t->getId()] = $t; }
            foreach($allPokedex as $p) { $this->loaded[Pokedex::tableName()][$p->getId()] = $p; }

            // getting all advantages and types relations
            $advantages = $this->executeStaticDictSelectRequest('Advantage');
            $pokedexHasType = $this->executeStaticDictSelectRequest('PokedexHasType');

            // creating advantages relations
            foreach($advantages as $a) {
                $t1 = $this->executeStaticSelectRequest(Type::class, $a["mainTypeId"])[0] ;
                $t2 = $this->executeStaticSelectRequest(Type::class, $a["againstTypeId"])[0] ;
                $t1->addAdvantage($t2, $a["modificator"]);
            }
            // creating types relations
            foreach($pokedexHasType as $p) {
                $poke = $this->executeStaticSelectRequest(Pokedex::class, $p["pokedexNumber"])[0] ;
                $type = $this->executeStaticSelectRequest(Type::class, $p["typeId"])[0] ;
                $poke->addType($type);
            }

            // Creating evolutions relation
            foreach($allPokedex as $p) {
                if ($p->evolutionNumber) {
                    $e = $this->executeStaticSelectRequest(Pokedex::class, $p->evolutionNumber)[0];
                    $p->setEvolution($e);
                }
            }
        }

        // ! Pdo Management
        private function getPdo() {
            if ($this->pdo == NULL) {$this->pdo = $this->connectDB();}
            return $this->pdo;
        }
        private function connectDB() {
            $dsn = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
            try {
                $db = new PDO($dsn, DB_USER, DB_PASS);
                return $db;
            } catch (PDOException $e) {
                echo "Error connecting database: " . $e->getMessage();
            }
            return false;
            
        }
        
        // ! Requests
        // ? Static data
        private function executeStaticSelectRequest($class, $id=NULL) {
            // Creating base sql statement
            $table = $class::tableName();
            if (isset($this->loaded[$table])) {
                if ($id) { return [$this->loaded[$table][$id]]; }
                else { return $this->loaded[$table]; }
            }
            else {
                $sql = 'SELECT * FROM ' . $table;
                if ($id) { $sql .= ' WHERE ' . $class::idColName() . ' = ' . $id; }
                $request = $this->getPdo()->query($sql);
                return $request->fetchAll(PDO::FETCH_CLASS, $table); 
            }
        }
        private function executeStaticDictSelectRequest($table) {
            $request = $this->getPdo()->query('SELECT * FROM ' . $table);
            return $request->fetchAll(PDO::FETCH_ASSOC);
        }

        // ? Non Static data
        private function executeSingleRowSelectRequest($class, $id) {
            // Creating base sql statement
            $table = $class::tableName();
            $sql = 'SELECT * FROM ' . $table . ' WHERE ' . $class::idColName() . ' = ' . $id;
            $request = $this->getPdo()->query($sql);
            return $request->fetchAll(PDO::FETCH_CLASS, $table); 
        }
        private function executeMultiplesRowSelectRequest($class, $field = NULL, $value = NULL) {
            // Creating base sql statement
            $table = $class::tableName();
            $sql = 'SELECT * FROM ' . $table;
            if ($field && $value) { $sql .= ' WHERE ' . $field . ' = ' . $value; }
            $request = $this->getPdo()->query($sql);
            return $request->fetchAll(PDO::FETCH_CLASS, $table); 
        }
        private function insert($class, $values) {
            $table = $class::tableName();
            $fields = [];
            $vals = [];
            foreach($values as $key => $v){
                $fields[] = "`" . $key . "`";
                if (is_string($v)) { $v = "'" . $v . "'"; }
                $vals[] = $v;
            }
            $field = " (" . join(", ", $fields) . ")";
            $vals = "(" . join(", ", $vals) . ")";
            $sql = 'INSERT INTO ' . $table . $field . " VALUES " . $vals;
            $request = $this->getPdo()->query($sql);
            return intval($this->getPdo()->lastInsertId());
        }
        private function update($class, $id, $values) {
            $table = $class::tableName();
            $vals = [];
            foreach($values as $key => $v){
                if (is_string($v)) { $v = "'" . $v . "'"; }
                $vals[] = "`" . $key . '` = ' . $v;
            }
            $vals = join(", ", $vals);
            $sql = 'UPDATE ' . $table . ' SET ' . $vals . " WHERE " . $class::idColName() . ' = ' . $id;
            $request = $this->getPdo()->query($sql);
        }

        // ! Public methods
        // ! - Types
        public function getAllTypes() {
            $all = $this->executeStaticSelectRequest(Type::class);
            return $all;
        }
        public function getType($id) {
            $all = $this->executeStaticSelectRequest(Type::class, $id);
            return count($all) > 0 ? $all[0] : NULL;
        }


        // ! - Pokedex
        public function getAllPokedex() {
            $all = $this->executeStaticSelectRequest(Pokedex::class);
            return $all;
        }
        public function getPokedex($id) {
            $all = $this->executeStaticSelectRequest(Pokedex::class, $id);
            return count($all) > 0 ? $all[0] : NULL;
        }

        // ! - User
        public function getUser($id) {
            $user = $this->executeSingleRowSelectRequest(User::class, $id);
            if (isset($user[0])) {
                $user = $user[0];
                foreach($this->getPokemons($user) as $pokemon) {
                    $user->addPokemon($pokemon);
                }
                return $user;
            }
            return NULL;
        }
        public function addUser($name, $pass, $starter) {
            $pass = User::cypherPassword($name, $pass);
            $id = $this->insert(User::class, ['name' => $name, 'pass' => $pass]);
            $this->addPokemon($id, $starter);
            return $this->getUser($id);
        }

        public function login($id, $pass) {
            $user = $this->executeSingleRowSelectRequest(User::class, $id);
            if (isset($user[0]) && $user[0]->isValid($pass)) {
                $token = $user[0]->makeNewToken();
                $this->update(User::class, $user[0]->getId(), ['token' => $token]);
                return $token;
            }
            return NULL;
        }

        // ! - Pokemons
        public function getPokemons($user) {
            $pokemons = $this->executeMultiplesRowSelectRequest(Pokemon::class, 'idUser', $user->getId());
            foreach($pokemons as $pokemon){ $pokemon->setPokedex($this->getPokedex($pokemon->numberPokedex)); }
            return $pokemons;
        }
        public function getPokemon($id) {
            $pokemon = $this->executeSingleRowSelectRequest(Pokemon::class, $id);
            if (isset($pokemon[0])) {
                $pokemon = $pokemon[0];
                $pokemon->setPokedex($this->getPokedex($pokemon->numberPokedex));
                return $pokemon;
            }
            return NULL;
        }
        public function addPokemon($userId, $starter) {
            $id = 25;
            if ($starter == 1) { $id = 1; }
            else if ($starter == 2) { $id = 4; }
            else if ($starter == 3) { $id = 7; }
            $pokedex = $this->executeStaticSelectRequest(Pokedex::class,$id)[0];
            $vals = [
                'name' => $pokedex->name, 
                'lvl' => 1, 'hp' => rand(1, $pokedex->hp),  
                'atk' => rand(1, $pokedex->atk), 
                'def' => rand(1, $pokedex->def), 
                'speed' => rand(1, $pokedex->speed), 
                'spe' => rand(1, $pokedex->spe), 
                'numberPokedex' => $pokedex->getId(), 
                'order' => 1, 
                'idUser' => $userId
            ];
            $id = $this->insert(Pokemon::class, $vals);
            return $this->getPokemon($id);
        }
    }
    
?>