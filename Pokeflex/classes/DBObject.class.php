<?php
    abstract class DBObject {
        abstract public static function tableName();
        abstract public static function idColName();
        abstract public function getId();
        abstract public function __toString();
        abstract public function toJSON();
    }
?>