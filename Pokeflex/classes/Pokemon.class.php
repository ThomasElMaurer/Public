<?php
    class Pokemon extends DBObject {
        // ! Class Methods
        public static function tableName() {return "Pokemon";}
        public static function idColName() {return "id";}
        
        // ! PDO Stuff 
        public $id;
        public $name;
        public $hp, $atk, $def, $speed, $spe, $lvl;
        public $numberPokedex;
        public $idUser;
        public $order;

        // ! Custom Stuff
        private $pokedex;
        public function setPokedex($pokedex) { $this->pokedex = $pokedex; }
        public function getPokedex() { return $this->pokedex; }

        private $owner;
        public function setOwner($owner) { $this->owner = $owner; }
        public function getOwner() { return $this->owner; }

        // ! Inherited Stuff
        public function getId() { return intval($this->id); } 
        public function __toString() { return $this->getId() . ": " . $this->name; }
        public function toJSON() {
            return [
                'id' => $this->getId(),
                'name' => $this->name,
                'owner' => $this->idUser,
                'pokedexId' => $this->pokedex->getId(),
                'pokedexName' => $this->pokedex->name,
                'order' => $this->order,
                'stats' => [
                    'lvl' => $this->hp,
                    'hp' => $this->hp,
                    'atk' => $this->atk,
                    'def' => $this->def,
                    'speed' => $this->speed,
                    'spe' => $this->spe
                ]
            ];
        }
    }
?>