<?php
    class Type extends DBObject {
        // ! Class Methods
        public static function tableName() {return "Type";}
        public static function idColName() {return "id";}
        
        // ! PDO Stuff 
        public $id;
        public $name;

        // ! Custom Stuff
        private $advantages;
        public function __construct(){ $this->advantages = []; }

        // ! - Advantages 
        public function addAdvantage($type, $value){ $this->advantages[$type->id] = floatval($value); }
        public function advantageFor($type) { return $this->advantages[$type->id]; }

        // ! Inherited Stuff
        public function getId() { return intval($this->id); }
        public function __toString() { return $this->name;}
        public function toJSON(){
            $json = [];
            $json["id"] = $this->id;
            $json["name"] = $this->name;
            $json["advantages"] = $this->advantages;
            return $json;
        } 
    }
?>