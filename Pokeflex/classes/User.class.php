<?php
    class User extends DBObject {
        // ! Class Methods
        public static function tableName() {return "User";}
        public static function idColName() {return "id";}
        public static function cypherPassword($name, $pass) { return sha1($pass . $name); }

        // ! PDO Stuff 
        public $id;
        public $name;
        public $pass;
        public $token;

        // ! Custom Stuff
        private $pokemons;
        public function addPokemon($pokemon) { 
            $this->pokemons[] = $pokemon; 
            $pokemon->setOwner($this);
        } 
        public function getPokemons() { return $this->pokemons; }
        public function isValid($pass) { return self::cypherPassword($this->name, $pass) == $this->pass; }
        public function makeNewToken() { 
            $str = "";
            for ($i = 0; $i < 64; $i++) { $str .= rand(0, 1000); }
            return sha1($str);
        }

        // ! Inherited Stuff
        public function getId() { return intval($this->id); } 
        public function __toString() { return $this->getId() . ": " . $this->name;}
        public function toJSON() {
            $json = ['id'=> $this->getId(), 'name' => $this->name];
            $pokemons = [];
            foreach($this->pokemons as $p) {
                $pokemons[] = $p->toJSON();
            }
            $json['pokemons'] = $pokemons;
            return $json;
        }
    }
?>