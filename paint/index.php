<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    // ! Imports
    // We start by importing the main config script
    require_once("config/main.conf.php");

    function sendObject($object) {
        // Sending Header
        header('Content-Type: application/json');
        // Sending JSON
        echo json_encode($object, JSON_PRETTY_PRINT);
    }

    
    $dm = new DatabaseManager();

    $action = NULL;
    if (isset($_GET['action'])) { $action = $_GET['action']; }
    if ($action == 'getAll') {
        sendObject($dm->getAllDrawings());
    }
    else if ($action == 'getById') {
        if (isset($_GET['id'])) {
            $res = $dm->getDrawingForId($_GET['id']);
            if ($res) { sendObject($res) ;}
            else { sendObject(['message' => 'Invalid id']); }
        }
        else { sendObject(['message' => 'No id provided']); }
    }
    else if ($action == 'send') {
        if (isset($_GET['author'])) {
            $input = file_get_contents('php://input');
            if (isset($input) && $input) {
                $id = $dm->addDrawing($_GET['author'], $input);
                sendObject(["id" => $id]);
            }
            else { sendObject(['message' => 'No data']); }
        }
        else { sendObject(['message' => 'No author']); }
    }
    else {
        sendObject(['message' => 'No action']); 
    }
    
?>